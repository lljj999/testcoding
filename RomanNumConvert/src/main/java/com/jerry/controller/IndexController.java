package com.jerry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jerry.service.RomanNumberService;

@Controller
public class IndexController {

	@RequestMapping("/index")
    public String index() {
		System.out.println("index-----");
        // return模板文件的名称，对应src/main/resources/templates/index.html
        return "index";  
    }
	
	@ResponseBody
	@RequestMapping(value="/convert",method=RequestMethod.POST)
	public String convert(String number,String status) {
		String result="";
		if(status.equals("atr")) {
			if(RomanNumberService.isDigit(number)){ //如果是整數
                int n = Integer.parseInt(number); //將字串轉為整數
                if(n>0&&n<4000000){ //如果n大於0且小於4000000
                	result=number + " = " + RomanNumberService.toRnums(n);
                }else{ //如果超過範圍
                	result="輸入的阿拉伯數字有誤！必須大於零且小於四百萬！";
                }
            }
		}
		
		if(status.equals("rta")) {
			int n = RomanNumberService.toAnums(number); //將羅馬字串轉為阿拉伯數字(數值)
            if(RomanNumberService.toRnums(n).equals(number)) { //判斷輸入的羅馬字串格式是否正確(轉成阿拉伯數字(數值)後再轉回羅馬字串，並與原本的字串做比較)
            	result=number + " = " + n;
            }else{
            	result="輸入的羅馬數字有誤！";
            }
		}
		
		
		return result;
	}
}
